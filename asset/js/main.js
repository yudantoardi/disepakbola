$(document).ready(function(){

    $(".search").click(function(s){
        s.stopPropagation();
        $(this).addClass("active");
        $(".search form input").focus();
    });
    $("body").click(function(s){
        $(".search").removeClass("active");
    });

    $(".burger").click(function(){
        $(".burger i").toggleClass("fa-bars fa-times");
        $(".menu").toggleClass("show");
    });

    $(window).scroll(function(){
        if ($(window).scrollTop() >= 80) {
            $('.bottom-bar').addClass('fixed');
            $(".master").addClass("header-fixed")
        }
        else {
            $('.bottom-bar').removeClass('fixed');
            $(".master").removeClass("header-fixed")
        }
    });

});